# Poom Penghiran 5913873

import sys


class Graph:
    vector = 0
    isVisited = False
    node = ""

    @classmethod
    def __init__(self):
        self.adjacent = []

    def setVisited(self):
        self.isVisited = True

    def getNode(self):
        return self.node


graph = []
# input file within python argument
with open(sys.argv[1], 'r') as f:
    inputFile = f.read()
rawFile = inputFile.splitlines()

# process file input
counter = 0
for i in range(0, len(rawFile), 2):
    graph.append(Graph())
    tempNode = str(rawFile[i])
    graph[counter].node = tempNode
    temp = rawFile[i + 1].split(",")
    graph[counter].adjacent = temp
    counter += 1


# BFS
def find_node_index(key):  # function to find index of the node within the graph
    for index in range(0, len(graph)):
        if graph[index].node == key:
            return index

    return "ERROR"


queue = []
result = []
currentVector = 0


searchKey = input("Enter search key: ")
start = input("Enter start key: ")

startIndex = find_node_index(start)
# Mark start as read

try:
    graph[startIndex].setVisited()
except TypeError:
    print("ENTER VALID START KEY!!")
queue.append(startIndex)


while True:

    if len(queue) == 0:
        print("NOT FOUND!!")
        break

    currentNode = graph[queue.pop(0)] # get current node from queue

    # comment this part to search all of the nodes
    if currentNode.node == searchKey:
        print("------")
        print(f"FOUND: {currentNode.node}")
        print(f'Required Vector: {currentNode.vector}')
        break

    # designated algorithm to travel node alphabetically reversed
    currentNode.adjacent.sort()
    currentNode.adjacent.reverse()

    tempVector = currentNode.vector + 1  # add vector to from current node

    for i in range(0, len(currentNode.adjacent)):
        tempAdjacentIndex = find_node_index(currentNode.adjacent[i])

        if not graph[tempAdjacentIndex].isVisited:
            graph[tempAdjacentIndex].setVisited()
            graph[tempAdjacentIndex].vector = tempVector
            queue.append(tempAdjacentIndex)
        # print(graph[tempAdjacentIndex].vector)
        # print("Not yet visited node put in queue")

    result.append(currentNode.node)

# Poom Penghiran 5913873

import sys


class Graph:
    vector = 0
    isVisited = False
    node = ""

    @classmethod
    def __init__(self):
        self.adjacent = list()
        self.path = list()

    def setVisited(self):
        self.isVisited = True

    def getNode(self):
        return self.node


graph = []
# input file within python argument
with open(sys.argv[1], 'r') as f:
    inputFile = f.read()
rawFile = inputFile.splitlines()

# process file input
counter = 0
for i in range(0, len(rawFile), 2):
    graph.append(Graph())
    tempNode = str(rawFile[i])
    graph[counter].node = tempNode
    temp = rawFile[i + 1].split(",")
    graph[counter].adjacent = temp
    graph[counter].adjacent.sort()  # designated algorithm to travel node alphabetically reversed
    graph[counter].adjacent.reverse()

    counter += 1

# DFS


def find_node_index(key):  # function to find index of the node within the graph
    for index in range(0, len(graph)):
        if graph[index].node == key:
            return index

    return "ERROR"


output = []

findKey = input("Enter search key: ")
startKey = input("Enter start key: ")

global isFound
isFound = False

startIndex = find_node_index(startKey)

graph[startIndex].setVisited()  # set start index to visited
output.append(graph[startIndex].node)

# DFS


def _dfs(node):
    current_node = graph[node]

    for index in range(0, len(current_node.adjacent)):  # iterate for all adjacent nodes

        index_node = graph[find_node_index(current_node.adjacent[index])]
        # print(f"index node now {index_node.node}")

        # comment this section if you don't want to find node
        if current_node.node == findKey:
            global isFound
            isFound = True
            break

        if not index_node.isVisited:
            temp_index = find_node_index(current_node.adjacent[index])
            graph[temp_index].setVisited()
            output.append(graph[temp_index].node)
            _dfs(temp_index)


def dfs():
    _dfs(startIndex)


if __name__ == '__main__':
    dfs()

    print(f"Node that visited: {output}")

    if isFound:
        print(f'Node {findKey} Found!!!')
    else:
        print(f'Node {findKey} not Found!!!')

## About

This project is partial fulfillment of Data Structure and Algorithms CS2201 (1/2017)

Department of Computer Science | Assumption University

### Input Format 
In order to run this project, the input files are required. The committed project contains sample set of input name 
> input.dat

To insert your new data set, you must have a graph node name with a list of its neighbors or adjacencies. The format of the input are as follows

> 
A --> name of the node
>
C,D,B --> its adjacency list

All of these are separated by line break, for more information you can see the file __input.dat__ for more detail

### Running a program

To run a program, you can just type the program name with the file name as a parameter like this...

`~python BFS.py data.dat`

Please note that this program requires Python 3.6 or higher in order to run properly

## Project Information

The choice of the project that I chose is...

* Breath First Search
* Depth First Search

## Created By
##### Poom Penghiran 5913873

